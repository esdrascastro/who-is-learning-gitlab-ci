import React from 'react';
import logo from './gitlab.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <a
          className="App-link"
          href="https://gitlab.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn GitLab CI
        </a>
      </header>
      <div className="participants">
        <h3>
          Add your name to the list to demonstrate you have completed the course.
        </h3>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Location</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Valentin</td>
              <td>@vdespa</td>
              <td>Romania 🇷🇴</td>
              <td>Thank you for taking this course.</td>
            </tr>
            <tr>
              <td>Anurag</td>
              <td>@budme</td>
              <td>USA</td>
              <td>Thank you for this amazing course.</td>
            </tr>
            <tr>
              <td>Saugata Bhattacharyya</td>
              <td>@saubhatta</td>
              <td>India IN</td>
              <td>Thank you for making this amazing course.</td>
            </tr>
            <tr>
              <td>Luiz Ricardo de Lima</td>
              <td>ricardolima</td>
              <td>Brazil 🇧🇷</td>
              <td>It was an amazing course full of new contents. I realize that there is A LOT to learn in this field.</td>
            </tr>
            <tr>
              <th>Don Ludemann</th>
              <th>@don803</th>
              <th>Kansas, USA</th>
              <th>Thank you for the course material.  <br />Completed: 2022-08-14</th>
            </tr>
            <tr>
              <td>Todd Seesz</td>
              <td>@toddseesz</td>
              <td>United States 🇺🇸</td>
              <td>Great course and teaching style </td>
            </tr>
            <tr>
              <td>Yovraj</td>
              <td>yuvv07</td>
              <td>India</td>
              <td>THANKYOU!! it was great learning from you uh made it easy for real</td>
            </tr>
            <tr>
              <td>Amit Mahato</td>
              <td>@Amitmahato</td>
              <td>Nepal 🇳🇵</td>
              <td>Thank you for making such an awesome course. I wanted to start with the DevOps but could not find where to start from. The CICD portion was the one that as a developer I have seen being used and hence was curios to learn that but I was not sure if I can jump directly on to that. This course has taught me lots of things and I guess this is the first milestone in the path to being a DevOps engineer that I have achieved. Thank you again, Valentin for making such an easy to follow course.</td>
            </tr>
            <tr>
              <td>Ramazan</td>
              <td>@rmzturkmen</td>
              <td>Nederlands</td>
              <td>It was really a step-by-step, well-prepared and explained course. Thank you Valentin!.</td>
            </tr>
            <tr>
              <td>Chris Williams</td>
              <td>@mistwire</td>
              <td>🌎</td>
              <td>Thank you for making this course ❤️🥰</td>
            </tr>
            <tr>
              <th>Bamba Deme</th>
              <th>@BambaDeme</th>
              <th>Dakar</th>
              <th>Think you! helped me understand gitlab and CI/CD! Exactly what i needed</th>
            </tr>
            <tr>
              <td>Noelle</td>
              <td>@NoelleinMN</td>
              <td>United States 🇺🇸</td>
              <td>Learned so much! Thank you, Valentin!</td>
            </tr>
            <tr>
              <th>Adrian</th>
              <th>@popaadrianvd</th>
              <th>Romania 🇷🇴</th>
              <th>Thank you Valentin! This was a great tutorial, I know feel a lot more confident working with CI/CD pipelines.</th>
            </tr>
            <tr>
              <th>Veneselin</th>
              <th>@veneselin</th>
              <th>Bulgaria</th>
              <th>Thank you!</th>
            </tr>
            <tr>
              <td>Fatih</td>
              <td>@fatihtepe</td>
              <td>Canada🇨🇦</td>
              <td>Thank you, this course was an amazing experience </td>
            </tr>
            <tr>
              <td>Ibrahima </td>
              <td>@biranbirane1210</td>
              <td>Sénégal🇸🇳 </td>
              <td>It was exciting and very interesting. Thank you 🙏 for this great content </td>
            </tr>
            <tr>
              <td>Jules</td>
              <td>@julesyoum</td>
              <td>Sénégal 🇸🇳 </td>
              <td>Very interesting content. Thank you very much </td>
            </tr>
            <tr>
              <td>Jesse</td>
              <td>@jpadillaa</td>
              <td>Colombia 🇨o</td>
              <td>Thank you, Valentine</td>
            </tr>
            <tr>
              <td>JBuck</td>
              <td>@buckdevment</td>
              <td>Cuba 🇨🇺</td>
              <td>Valentine for president! This course creates better citizens!!</td>
            </tr>
            <tr>
              <td>Anna</td>
              <td>@aakojyan</td>
              <td>Armenia</td>
              <td>This was a great and very informative course. Thank you!</td>
            </tr>
            <tr>
              <td>Billy</td>
              <td>@BillyGrande</td>
              <td>Greece</td>
              <td>Very helpful course! Thank you for providing us such resources.</td>
            </tr>
            <tr>
              <td>Vinícius</td>
              <td>@bl4cktux89</td>
              <td>Brazil 🇧🇷</td>
              <td>Your coarse is amazing and i learned a lot!</td>
            </tr>
            <tr>
              <td>Pedro</td>
              <td>@pedro.calvo</td>
              <td>Mexico 🇲🇽</td>
              <td>Excellent course! Amazing to learn automation pipelines, gitlab CI/CD  and Devops cycle</td>
            </tr>
            <tr>
              <td>Innocent Onyemaenu</td>
              <td>@rockcoolsaint</td>
              <td>Nigeria 🇳🇬</td>
              <td>Thank you for taking your time to create this wonderful and easy to follow along course.</td>
            </tr>
            <tr>
              <th>Choustoulakis Nikolaos</th>
              <th>@NikosChou</th>
              <th>Greece 🇬🇷</th>
              <th>Thank you for sharing!</th>
            </tr>
            <tr>
              <th>William F. Silva</th>
              <th>@willianfrancas</th>
              <th>Brazil 🇧🇷</th>
              <th>
                For a long time I was looking for a content to learn about DevOps CI/CD, and your course 
                from freecodecamp youtube channel, helped me a lot! I am working as front end developer. 
                Now I have more than I need to delivery content at work or in side projects.
                I will let others people to know about you!
                Thank you so much, Despa!
                You deserve more than words!
              </th>
            </tr>
            <tr>
              <td>Muhammad Abutahir</td>
              <td>@abutahir</td>
              <td>India</td>
              <td>This is a must watch and implement course for every developer! Thank you so much 💙.</td>
            </tr>
            <tr>
              <td>KagemniKarimu</td>
              <td>@KagemniKarimu</td>
              <td>USA (Afrikan)</td>
              <td>Awesome course. I enjoyed it.</td>
            </tr>
            <tr>
              <td>Leo</td>
              <td>@leonel.barriga</td>
              <td>Argentina🇦🇷</td>
              <td>Thanks for the course Valentin. It was great!</td>
            </tr>
            <tr>
              <td>Phat NV</td>
              <td>@phat-nv</td>
              <td>Viet Nam</td>
              <td>Thank you for sharing this course.</td>
            </tr>
            <tr>
              <td>Rahul Gupta</td>
              <td>@rahulgupta141998</td>
              <td>India</td>
              <td>Thank you for the course</td>
            </tr>
            <tr>
              <td>Thomas Poth</td>
              <td>@thomas.poth</td>
              <td>Germany</td>
              <td>Valentin, you have made really great content. It fits perfectly as I currently have many GitLab projects running. One of the last secrets was GitLabc&apos;s CI/CD system. Thank you for this course!</td>
            </tr>
            <tr>
              <td>Santosh Dawanse</td>
              <td>@dawanse-santosh</td>
              <td>Nepal 🇳🇵</td>
              <td>Great content, Thank you for offering this course 💙.</td>
            </tr>
            <tr>
              <td>Axel Somerseth Cordova</td>
              <td>@axelsomerseth</td>
              <td>Honduras 🇭🇳</td>
              <td>Thank you for this course. I learned a lot! 🚀</td>
            </tr>
            <tr>
              <td>Eric Daly</td>
              <td>@linucksrox</td>
              <td>United States</td>
              <td>Thanks for this course, it helped me start making my own pipelines already.</td>
            </tr>
            <tr>
              <td>Maciej Friedel</td>
              <td>@ippolit</td>
              <td>Poland</td>
              <td>Thanks to this course I had a successful weekend</td>
            </tr>
            <tr>
              <td>Muiz Uvais</td>
              <td>@Muiz U</td>
              <td>Sri Lanka</td>
              <td>I found the course to be extremely useful, and informative. Thanks for making such great content</td>
            </tr>
            <tr>
              <td>Mohamed Nasrullah</td>
              <td>@nasr16</td>
              <td>India</td>
              <td>Really great course. Learned a lot in this one video. Thank you for offering this course 💙.</td>
            </tr>
            <tr>
              <td>Alex</td>
              <td>@AlexAg97</td>
              <td>Ecuador 🇪🇨</td>
              <td>I really appreciate this course, it is a light on the dark path of DevOps.</td>
            </tr>
            <tr>
              <td>Norman David</td>
              <td>@david_mbuvi</td>
              <td>Kenya</td>
              <td>I am really grateful for this course. Thank you for offering this course.</td>
            </tr>
            <tr>
              <td>Paul</td>
              <td>@phall659</td>
              <td>USA</td>
              <td>Great course! Thanks for taking time to put it together.</td>
            </tr>
            <tr>
              <td>Mallikarjun Reddy Dorsala</td>
              <td>@mallikarjun.reddy1</td>
              <td>India</td>
              <td>Thank you. Enjoyed your teaching</td>
            </tr>
            <tr>
              <td>Navi Singh</td>
              <td>@navisingh</td>
              <td>India</td>
              <td>I am very grateful. There are so many things to learn in this course for beginners like me. Superb instructor.</td>
            </tr>
            <tr>
              <td>Ezekiel Kolawole</td>
              <td>@eazybright</td>
              <td>Nigeria 🇳🇬</td>
              <td>I learnt alot from this course. Thanks so much!</td>
            </tr>
            <tr>
              <td>David</td>
              <td>@david1058</td>
              <td>Kenya 🇰🇪 / USA</td>
              <td>Alright Kubernetes, you&#39;re next!.</td>
            </tr>
            <tr>
              <td>Andrii Tugai</td>
              <td>@atugai</td>
              <td>Ukraine 🇺🇦</td>
              <td>Thank you for the course Valentin 😀 I learnt a lot</td>
            </tr>
            <tr>
              <th>Ebenezer Makinde</th>
              <th>@ebenezermakinde</th>
              <th>Nigeria 🇳🇬</th>
              <th>This course was just tailored to purpose. Great content. Thanks for this course</th>
            </tr>
            <tr>
              <td>Kostiantyn</td>
              <td>@kofesenko</td>
              <td>Ukraine</td>
              <td>Thank you for this course. I found it to be the perfect resource to master my knowledge of GitLab as a platform and the features of GitLab CI/CD in particular.</td>
            </tr>
            <tr>
              <td>Jose Cano</td>
              <td>@jskno</td>
              <td>Spain</td>
              <td>Great course. Clear explanations, straight to the point !! Keep teaching, please</td>
            </tr>
            <tr>
              <td>Tomasz Fijarczyk</td>
              <td>@t0mmili</td>
              <td>Poland 🇵🇱</td>
              <td>Comprehensive, pleasantly presented course. It was actually understandable from beginning till end. Thank you Valentin!</td>
            </tr>
            <tr>
              <td>Ayodele Ademeso</td>
              <td>@ayodele-spencer</td>
              <td>Nigeria 🇳🇬</td>
              <td>This course was precise and easy to follow from start to finish. I miss it already. I was also able to setup a CI for a project I created earlier using this course. Thank you Valentin!</td>
            </tr>
            <tr>
              <td>Christian Aluya</td>
              <td>@caluya</td>
              <td>United Kingdom UK</td>
              <td>The best course on gitlab-ci so far, you are the best. Thank you a million times over!</td>
            </tr>
            <tr>
              <td>A.Iusupov</td>
              <td>@alikyusupov</td>
              <td>Uzbekistan 🇺🇿</td>
              <td>So precious course! Thanks Valentin</td>
            </tr>
            <tr>
              <td>Patrick Santino</td>
              <td>@sotobakar</td>
              <td>Indonesia 🇮🇩</td>
              <td>Learnt many things in this course. Now, I am going to start my journey to automate my development processes. Appreciate the course @FCC and @Valentin.</td>
            </tr>
            <tr>
              <th>Ivan Chen</th>
              <th>@chen-chao-wei</th>
              <th>Taiwan 🇹🇼</th>
              <th>This course is easy to understand and very helpful. Thank you for offering this course😀</th>
            </tr>
            <tr>
              <th>Lilian Etchepare</th>
              <th>@letchepare</th>
              <th>France 🇫🇷</th>
              <th>I loved the way this course was explained. Glad it went into my youtube recommendations. Thanks a lot Valentin for your work.</th>
            </tr>
            <tr>
              <th>Sarakorn Sakol</th>
              <th>@shurricanex</th>
              <th>Cambodia</th>
              <th>Learnt so many stuffs I never knew before. It is a wonderful course, and I was enjoying learning it a lot, thanks for creating this such a precious course.</th>
            </tr>
            <tr>
              <th>Deepak</th>
              <th>@mini2dust</th>
              <th>India</th>
              <th>THANK YOU!! for putting this course in such easily understandable way, ur teach was awesome</th>
            </tr>
            <tr>
              <th>Chris Brook</th>
              <th>@cbrman74</th>
              <th>UK</th>
              <th>Brilliant course, I have learned so much. Thanks a lot Valentin.</th>
            </tr>
            <tr>
              <th>Gabriel Francia</th>
              <th>@cindrmon</th>
              <th>Philippines</th>
              <th>This has been one of the most helpful courses I have ever took to kickstart my DevOps path, and I thank you so much for providing such content! I&apos;m looking forward to using this with agile scrum methodologies and better project management for future projects, and to learn more about the world of DevOps with Gitlab!</th>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default App;
